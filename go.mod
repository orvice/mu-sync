module github.com/orvice/v2ray-mu

require (
	github.com/catpie/musdk-go v0.0.0-20220203130438-2a16f5893029
	github.com/google/gops v0.3.22
	github.com/orvice/v2ray-manager v0.0.0-20220622031109-26a26a8cd40b
	github.com/p4gefau1t/trojan-go v0.10.6
	github.com/v2fly/v2ray-core/v5 v5.0.6
	github.com/weeon/contract v0.0.0-20190520152601-a4ee53bdb563
	github.com/weeon/log v0.0.0-20210217051817-63fe9a730962
	github.com/weeon/utils v0.0.0-20210724174708-113b40b2e152
	go.uber.org/zap v1.18.1
	google.golang.org/grpc v1.46.0
)

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/dgryski/go-metro v0.0.0-20200812162917-85c65e2d0165 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-redis/redis/v8 v8.5.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/pires/go-proxyproto v0.6.2 // indirect
	github.com/riobard/go-bloom v0.0.0-20200614022211-cdc8013cb5b3 // indirect
	github.com/seiflotfy/cuckoofilter v0.0.0-20220312154859-af7fbb8e765b // indirect
	github.com/v2fly/ss-bloomring v0.0.0-20210312155135-28617310f63e // indirect
	github.com/v2fly/v2ray-core/v4 v4.44.0 // indirect
	github.com/xtaci/smux v1.5.15 // indirect
	go.opentelemetry.io/otel v0.16.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.0.0-20220325170049-de3da57026de // indirect
	golang.org/x/sys v0.0.0-20220325203850-36772127a21f // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

go 1.17
